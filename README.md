This is a test project of a basic record adding/retrieval system.

This example has been written using CoreData for persistence.

Please see my other repo SQLiteExample which is the same project model, though uses SQLite instead.

(c) Jim Tierney 2015
