//
//  AllRecordTableViewController.h
//  CoreDataExample
//
//  Created by Jim Tierney on 13/01/2015.
//  Copyright (c) 2015 apjam ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AllRecordTableViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSManagedObjectContext *passedContext;

@end
