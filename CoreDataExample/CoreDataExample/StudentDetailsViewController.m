//
//  StudentDetailsViewController.m
//  CoreDataExample
//
//  Created by Jim Tierney on 13/01/2015.
//  Copyright (c) 2015 apjam ltd. All rights reserved.
//

#import "StudentDetailsViewController.h"
#import "Student+Utils.h"
#import "AppDelegate.h"
#import "AllRecordTableViewController.h"

@interface StudentDetailsViewController ()

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@implementation StudentDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.managedObjectContext = [[AppDelegate alloc]managedObjectContext];

}

- (IBAction)findDataButtonPressed:(id)sender {

    
        NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Student"];
        [request setPredicate:[NSPredicate predicateWithFormat:@"regNumber = %@", self.findByRegNumberTextField.text]];
        NSError *error = nil;
        NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];
        
        if ([results count]>0) {
            Student *student = results[0];
            self.regNumberTextField.text = student.regNumber;
            self.nameTextField.text = student.name;
            self.departmentTextField.text = student.department;
            self.yearTextField.text = student.year;
        }else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Data not found"
                                                       message:nil
                                                      delegate:nil
                                             cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        self.regNumberTextField.text = @"";
        self.nameTextField.text = @"";
        self.departmentTextField.text = @"";
        self.yearTextField.text = @"";
    }
    
    [self.view endEditing:YES];
}

- (IBAction)saveDataButtonPressed:(id)sender {
    
    NSString *alertString = @"Data Insertion failed";
    
    Student *student = nil;
    
    if (self.regNumberTextField.text.length>0 &&self.nameTextField.text.length>0 &&self.departmentTextField.text.length>0 &&self.yearTextField.text.length>0) {
        
        NSDictionary *details = @{@"regNumber":     self.regNumberTextField.text,
                                  @"name":          self.nameTextField.text,
                                  @"department":    self.departmentTextField.text,
                                  @"year":          self.yearTextField.text};
        
        student = [Student studentRecordWithDetails:details inContext:self.managedObjectContext];
        
        NSError *error = nil;
        BOOL result = [self.managedObjectContext save:&error];
        if (!result) {
            NSLog(@"Error saving data: %@", [error localizedDescription]);
        }

    }else{
        alertString = @"Enter all fields";
    }
    
    if (!student) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertString
                                                       message:nil
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil, nil];
        [alert show];
    }
    
}


#pragma mark - Textfield delegate methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField==self.yearTextField||textField==self.departmentTextField) {
        
        self.horizontalAlighnmentConstraint.constant += 70;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField==self.yearTextField||textField==self.departmentTextField) {
        self.horizontalAlighnmentConstraint.constant -= 70;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"allRecords"]) {
        AllRecordTableViewController *detailViewController = segue.destinationViewController;
        detailViewController.passedContext = self.managedObjectContext;
    }
}

@end
