//
//  StudentDetailsViewController.h
//  CoreDataExample
//
//  Created by Jim Tierney on 13/01/2015.
//  Copyright (c) 2015 apjam ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentDetailsViewController : UIViewController<UITextFieldDelegate, NSURLConnectionDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *horizontalAlighnmentConstraint;

@property (weak, nonatomic) IBOutlet UITextField *regNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *departmentTextField;
@property (weak, nonatomic) IBOutlet UITextField *yearTextField;

@property (weak, nonatomic) IBOutlet UITextField *findByRegNumberTextField;

- (IBAction)findDataButtonPressed:(id)sender;
- (IBAction)saveDataButtonPressed:(id)sender;
@end
