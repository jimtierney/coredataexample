//
//  Student+Utils.h
//  CoreDataExample
//
//  Created by Jim Tierney on 13/01/2015.
//  Copyright (c) 2015 apjam ltd. All rights reserved.
//

#import "Student.h"

@interface Student (Utils)

+(instancetype)studentRecordWithDetails:(NSDictionary*)details inContext:(NSManagedObjectContext*)context;

+(NSArray*)getAllStudentRecords:(NSManagedObjectContext*)context;

@end
