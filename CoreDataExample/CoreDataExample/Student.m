//
//  Student.m
//  CoreDataExample
//
//  Created by Jim Tierney on 13/01/2015.
//  Copyright (c) 2015 apjam ltd. All rights reserved.
//

#import "Student.h"


@implementation Student

@dynamic regNumber;
@dynamic name;
@dynamic department;
@dynamic year;

@end
