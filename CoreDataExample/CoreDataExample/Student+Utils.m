//
//  Student+Utils.m
//  CoreDataExample
//
//  Created by Jim Tierney on 13/01/2015.
//  Copyright (c) 2015 apjam ltd. All rights reserved.
//

#import "Student+Utils.h"

@implementation Student (Utils)

+(instancetype)studentRecordWithDetails:(NSDictionary *)details inContext:(NSManagedObjectContext *)context{
    
    Student *entity;
    
    NSString *entityId = [details objectForKey:@"regNumber"];
    
    NSFetchRequest *request = [Student fetchRequestForStudentWithId:entityId];
    
    NSError *error;
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    if (error!=nil) {
        NSLog(@"Error retrieving Student with Id: %@. Description: %@", entityId, error.description);
    }
    
    NSInteger resultsCount = [results count];
    
    if (resultsCount==0) {
        NSLog(@"Reg number not found, creating new");
        entity = [NSEntityDescription insertNewObjectForEntityForName:@"Student" inManagedObjectContext:context];
        [entity updateWithDetails:details];
    }else if (resultsCount==1){
        NSLog(@"Reg number exists, updating record");
        entity = results[0];
        [entity updateWithDetails:details];
    }else{
        NSLog(@"Unexpected results for Studen with RegNumber: %@ as results count = %ld", entityId, (long)resultsCount);
    }
    return entity;
}

+(NSFetchRequest*)fetchRequestForStudentWithId:(NSString*)entityId{
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Student"];

    [request setPredicate:[NSPredicate predicateWithFormat:@"regNumber = %@", entityId]];
    return request;
}

+(NSArray*)getAllStudentRecords:(NSManagedObjectContext *)context{
    //This method empty just now as will be used for further writing of project
    return nil;
}

-(void) updateWithDetails:(NSDictionary*)details{

    if ([details objectForKey:@"regNumber"]) {
        [self setRegNumber:[details objectForKey:@"regNumber"]];
    }
    if ([details objectForKey:@"name"]) {
        [self setName:[details objectForKey:@"name"]];
    }
    if ([details objectForKey:@"department"]) {
        [self setDepartment:[details objectForKey:@"department"]];
    }
    if ([details objectForKey:@"year"]) {
        [self setYear:[details objectForKey:@"year"]];
    }
}

@end
