//
//  Student.h
//  CoreDataExample
//
//  Created by Jim Tierney on 13/01/2015.
//  Copyright (c) 2015 apjam ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Student : NSManagedObject

@property (nonatomic, retain) NSString * regNumber;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * department;
@property (nonatomic, retain) NSString * year;

@end
